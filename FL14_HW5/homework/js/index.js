const _root = document.querySelector('body');
let list = document.createElement('div');
let userIdArray = [];
let idArray = [];
let titleArray = [];
let completedArray = [];
_root.appendChild(list);

let spinner = document.querySelector('.loader');
spinner.style.display = 'none';

function editData(editElement, id) {
    let edit = document.createElement('SPAN');
    let eText = document.createTextNode('\u270E');
    edit.className = 'edit';
    edit.appendChild(eText);
    edit.onclick = function () {
        let p = prompt('Edit your entry');
        if(p === ' ' || !p) {
            alert('Wrong parametrs!');
            return;
        }
        editElement.innerText = p;
        fetch(`https://jsonplaceholder.typicode.com/user/${id}`, {
            method: 'PUT',
            body: JSON.stringify({
                id: id,
                title: 'foo',
                body: 'bar',
                userId: 1
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            }
        })
            .then((response) => response.json())
    }
    return edit;
}

function postsDataAppend(postsDataElement) {
    let container = document.createElement('div');
    container.style.padding = '10px';
    let userId = document.createElement('p');
    let id = document.createElement('p');
    let title = document.createElement('p');
    let body = document.createElement('p');

    userId.innerHTML = postsDataElement.userId;
    id.innerHTML = postsDataElement.id;
    title.innerHTML = postsDataElement.title;
    body.innerHTML = postsDataElement.body;

    container.appendChild(userId);
    container.appendChild(id);
    container.appendChild(title);
    container.appendChild(body);
    list.appendChild(container);
}

function commentsDataAppend(commentsDataElement) {
    let container = document.createElement('div');
    container.style.padding = '10px';
    let postId = document.createElement('p');
    let id = document.createElement('p');
    let name = document.createElement('p');
    let email = document.createElement('p');
    let body = document.createElement('p');

    postId.innerHTML = commentsDataElement.postId;
    id.innerHTML = commentsDataElement.id;
    name.innerHTML = commentsDataElement.name;
    email.innerHTML = commentsDataElement.email;
    body.innerHTML = commentsDataElement.body;

    container.appendChild(postId);
    container.appendChild(id);
    container.appendChild(name);
    container.appendChild(email);
    container.appendChild(body);
    list.appendChild(container);
}

async function getPosts(node){
    const posts = await fetch(`https://jsonplaceholder.typicode.com/posts/${node.dataset.id}`);
    const postsData = await posts.json();
    await postsDataAppend(postsData);
    spinner.style.display = 'none';
}

async function getComments(node) {
    const comments = await fetch(`https://jsonplaceholder.typicode.com/comments/${node.dataset.id}`);
    const commentsData = await comments.json();
    await commentsDataAppend(commentsData);
    spinner.style.display = 'none';
}

function redirect(node){
    spinner.style.display = 'block';
    //document.location.href = '/';
    list.innerHTML = '';
    getComments(node);
    getPosts(node);
}

function appendData(data){
    let container = document.createElement('div');
    container.style.padding = '10px';
    let cText = document.createTextNode('\u274E');
    let close = document.createElement('SPAN'); 

    let idNode = document.createElement('SPAN');
    let nameNode = document.createElement('SPAN');
    let userNameNode = document.createElement('SPAN');
    userNameNode.className = 'username';
    let emailNode = document.createElement('SPAN');
    let phoneNode = document.createElement('SPAN');
    let websiteNode = document.createElement('SPAN');

    let idText = document.createTextNode('  ID: ');
    let nameText = document.createTextNode('  Name: ');
    let userNameText= document.createTextNode(' Username: ');
    let emailText = document.createTextNode('  Email: ');
    let phoneText = document.createTextNode('  Phone: ');
    let websiteText = document.createTextNode('  Website: ');

    idNode.innerHTML = data.id;

    nameNode.innerHTML = data.name;
    nameNode.appendChild(editData(nameNode, data.id));

    userNameNode.innerHTML = data.username;
    let redirectUserNameNode = editData(userNameNode, data.id);
    userNameNode.dataset.id = data.id;
    userNameNode.appendChild(redirectUserNameNode);

    emailNode.innerHTML = data.email;
    emailNode.appendChild(editData(emailNode, data.id));

    phoneNode.innerHTML = data.phone;
    phoneNode.appendChild(editData(phoneNode, data.id));

    websiteNode.innerHTML = data.website;
    websiteNode.appendChild(editData(websiteNode, data.id));

    close.className = 'close';
    close.appendChild(cText);
    close.onclick = function () {
        spinner.style.display = 'block';
        this.parentElement.style.display = 'none';
        fetch(`https://jsonplaceholder.typicode.com/user/${data.id}`, {
            method: 'DELETE'
        })
        spinner.style.display = 'none';
    }

    container.appendChild(close);
    container.appendChild(idNode);
    container.appendChild(nameNode);
    container.appendChild(userNameNode);
    container.appendChild(emailNode);
    container.appendChild(phoneNode);
    container.appendChild(websiteNode);
    container.insertBefore(idText, idNode);
    container.insertBefore(nameText, nameNode);
    container.insertBefore(userNameText, userNameNode);
    container.insertBefore(emailText, emailNode);
    container.insertBefore(phoneText, phoneNode);
    container.insertBefore(websiteText, websiteNode);
    list.appendChild(container);
}

const request = async () => {
    spinner.style.display = 'block';
    const response = await fetch('https://jsonplaceholder.typicode.com/users');
    const data = await response.json();
    for (let i = 0; i < data.length; i++) {
        await appendData(data[i]);
    }
    spinner.style.display = 'none';
}

list.addEventListener('click', (event) => {
    if (event.target.className === 'username'){
        redirect(event.target);
    }
}, false);

request();