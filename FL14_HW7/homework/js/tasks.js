const maxElement = (array) => {
    return Math.max(...array);
}

const copyArray = (array) => {
    let arrayOut = [...array];
    return arrayOut;
}

const addUniqueId = (obj) => {
    let returnedTarget = {};
    Object.assign(returnedTarget, obj);
    returnedTarget.name = Symbol(obj.name||1);
    return returnedTarget;
}

const regroupObject = (obj) => {
    let returnedTarget = {
        university: obj.details.university,
        user: {
            age: obj.details.age,
            firstName: obj.name,
            id: obj.details.id
        }
    };
    return returnedTarget;
}

const findUniqueElements = (array) => { 
    return [...new Set(array)];
}

let noMagic = -4;

const hideNumber = (str) => {
    let last4Digits = str.slice(noMagic);
    let maskedNumber = last4Digits.padStart(str.length, '*');
    return maskedNumber;
}

const err = msg => {
 throw Error(msg) 
};
const hide = function (str = err('Phone number is undefined')) {
    let last4Digits = str.slice(noMagic);
    let maskedNumber = last4Digits.padStart(str.length, '*');
    return maskedNumber;
}

function sortApiName(url = 'https://jsonplaceholder.typicode.com/users'){
    fetch(url)
        .then(response => response.json())
        .then(data => {
            let arrayOut = data.map(a => a.name);
            console.log(arrayOut.sort());
        });
}

async function sortApiNameAsyncAwait(url = 'https://jsonplaceholder.typicode.com/users') {
    let response = await fetch(url);
    let users = await response.json();
    let arrayOut = users.map(a => a.name);
    console.log(arrayOut.sort());
}