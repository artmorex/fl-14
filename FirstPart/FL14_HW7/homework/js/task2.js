let magicNum = 2;

function isNullOrWhiteSpace(str) {
	return !str || str.length === 0 || /^\s*$/.test(str);
}

function isOdd(num) {
	return num % magicNum;
}

function myFunction() {
	let strWord = prompt('Please enter the word', 'EPAM');
	if(isNullOrWhiteSpace(strWord)){
		alert('Invalid value');
	} else{		
		let arrWord = strWord.split('');
		if(isOdd(arrWord.length)){
			let middleOdd = Math.floor(arrWord.length/magicNum);
			alert(arrWord[middleOdd]);
		} else{
			let middleEven = Math.floor(arrWord.length/magicNum);
			if(arrWord[middleEven-1]===arrWord[middleEven]){
				alert('Middle characters are the same');
			} else{
				alert(arrWord[middleEven-1]+arrWord[middleEven]);
			}
		}
	}
}