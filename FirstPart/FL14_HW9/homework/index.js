// 1. Convert //
function convert(convArray){
	for(let i = 0; i<convArray.length; i++){
		if(typeof convArray[i] === 'string'){
			convArray[i]= +convArray[i];
		} else if(typeof convArray[i] === 'number'){
			convArray[i]=String(convArray[i]);
		}
	}
}

// 2. function executeforEach //
function executeforEach(arr, func) {
	for(let i = 0; i<arr.length; i++){
		func(arr[i]);
	}
}

// 3. function returns transformed array
function mapArray(arr, func){
    let transformArray = [];
    executeforEach(arr, function(el) {
        transformArray.push(func(el))
    });
    return transformArray;
}
// console.log(mapArray([2, '5', 8], function(el){
//     if (typeof el === "string") {
//         el = + el;
//     }
//     return el + 3;
// }));

// 4. function  returns  filtered  array 
function filterArray(arr, func){
	let filterArray = [];
    executeforEach(arr, function(el) {
		if(func(el)){
			filterArray.push(el);
		}
    });
    return filterArray;
}
// console.log(filterArray([2, 5, 8], function(el) { return el % 2 === 0 }));

// 5. function returns the value position
function getValuePosition(arr, val){
	for(let i = 0; i<arr.length; i++){
		if(arr[i] === val) {
			return ++i; 
		}
	}
	return false;
}
// console.log(getValuePosition([2, 5, 8], 8)); // returns 3  
// console.log(getValuePosition([12, 4, 6], 1)); // returns false 

// 6. function that reverses the string value passed into it 
function flipOver(str){
	let reverseStr = '';
    for (let i = str.length - 1; i >= 0; i--) { 
        reverseStr += str[i];
    }
    return reverseStr;
}
// console.log(flipOver('hey world')); // 'dlrow yeh'

//7. function creates an array from the given range of numbers
function makeListFromRange(arr){
	let listArray = [];
    for (let i = arr[0]; i <= arr[1]; i++) { 
        listArray.push(i);
    }
    return listArray;
}
// console.log(makeListFromRange([2, 7])); // [2, 3, 4, 5, 6, 7] 

//8. function returns new array of values by passed key name.  
function getArrayOfKeys(objArray, key){
	let arrayOfKeys = [];
	executeforEach(objArray, function(el){
		arrayOfKeys.push(el[key]);
	});
    return arrayOfKeys;
}
const fruits = [
  { name: 'apple', weight: 0.5 }, 
  { name: 'pineapple', weight: 2 } 
];
// console.log(getArrayOfKeys(fruits, 'name'));

// 9. function returns total weight of all items
function getTotalWeight(objArray){
	let totalWeight = 0;
	executeforEach(objArray, function(el){
		totalWeight += el.weight;
	});
    return totalWeight;
}
const basket = [ 
  { name: 'Bread', weight: 0.3 }, 
  { name: 'Coca-Cola', weight: 0.5 }, 
  { name: 'Watermelon', weight: 8 } 
]; 
// console.log(getTotalWeight(basket));

// 10.  Write a function which returns a day number that was some amount of days ago from the passed date.
function getPastDay(dateInput, diffVal){
	let dateOutput = new Date(dateInput);
	dateOutput.setDate(dateInput.getDate() - diffVal);
	let ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(dateOutput);
	let mo = new Intl.DateTimeFormat('en', { month: 'short' }).format(dateOutput);
	let da = new Intl.DateTimeFormat('en', { day: 'numeric' }).format(dateOutput);
	return `${da}, (${da} ${mo} ${ye})`;
}
// const date = new Date(2020, 0, 2); 
// console.log(getPastDay(date, 1)); // 1, (1 Jan 2020) 
// console.log(getPastDay(date, 2)); // 31, (31 Dec 2019) 
// console.log(getPastDay(date, 365)); // 2, (2 Jan 2019)

// 11. Write a function that formats a date in such format "YYYY/MM/DD HH:mm". 
function formatDate(dateInput){
	let ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(dateInput);
	let mo = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(dateInput);
	let da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(dateInput);
	let hr = new Intl.DateTimeFormat('en', { hour: '2-digit', hour12: false }).format(dateInput);
	let min = new Intl.DateTimeFormat('en', { minute: '2-digit' }).format(dateInput);
	return `${ye}/${mo}/${da} ${hr}:${min}`;
}
// console.log(formatDate(new Date('6/15/2019 09:15:00'))); // "2019/06/15 09:15" 
// console.log(formatDate(new Date())); // "2020/04/07 12:56" // gets current local time 