function isEquals(firstValue, secondValue){
	return firstValue===secondValue;
}
// console.log(isEquals(3,3));

function numberToString(numberValue){
	return numberValue.toString();
}
// console.log(typeof numberToString(1234));

function storeNames(...args){
	return Array.from(args);
}
//console.log(storeNames('Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy'));

function getDivision(firstValue, secondValue){
	if(firstValue<secondValue){
		let temp = firstValue;
		firstValue = secondValue;
		secondValue = temp;
	}
	let result = firstValue/secondValue;
	if(result<1){
		return false;
	} else{	
		return result;
	}
}
// console.log(getDivision(4,1));
// console.log(getDivision(2,8));

function negativeCount(numberArray){
	let result = 0;
	numberArray.forEach(function(element){
		if(element<0){
			result++;
		}
	});
	return result;
}
// console.log(negativeCount([4, 3, 2, 9]));
// console.log(negativeCount([0, -3, 5, 7]));

function letterCount(str, letter){
	//let result = (str.match(/[letter]/g)||[]).length; //wokrs fine, but eslint doesn't see parameter-letter //eslint error
	let regex = new RegExp( letter, 'g' );
	let result = (str.match(regex)||[]).length;
	return result;
}
// console.log(letterCount("Marry", "r")); //2
// console.log(letterCount("Barny", "y")); //1
// console.log(letterCount("", "z")); //0

let noMagic8 = 8;
let noMagic3 = 3;
function countPoints(arr){
	let pointsCounter = 0, splitArr;
	for(let i = 0; i<noMagic8; i++){
		splitArr = arr[i].split(':');
		if(Number(splitArr[0])>Number(splitArr[1])){
			pointsCounter += noMagic3;
		}
		if(splitArr[0]===splitArr[1]){
			pointsCounter++;
		}
	}
	return pointsCounter;
}
// console.log(countPoints(['100:90', '110:98', '100:100', '95:46', '54:90', '99:44', '90:90', '111:100'])); // => 17