let noMagic_2 = 2;

function heapSort(arr) {
    let len = arr.length,
        end = len - 1;

    heapify(arr, len);

    while(end > 0) {
        swap(arr, 0, end); //end
        siftDown(arr, 0, end);
        end--; //n end--
    }
    return arr;
}
function heapify(arr, len) {
    let mid = Math.floor(len / noMagic_2 - 1); //len / 2 - 1
    while (mid >= 0) { //>=
        siftDown(arr, mid, len); //len
        mid--; //mid--
    }
}
function siftDown(arr, start, end) {
    let root = start;

    while(start < end) { //start < end

        let child = noMagic_2*start + 1, //+1 //variable in while cycle
            toSwap = child + 1; //child+1 //variable in while cycle
        if (child < end && arr[child] > arr[root]){
            root = child;
        } // replace if conditions
        if (toSwap < end && arr[toSwap] > arr[root]){
            root = toSwap;
        } 
        if (root === start){ // == root // no need else
            return;
        }
        swap(arr,start, root);
        start = root;
    }
}
function swap(arr, i, j) {
    let temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
}

//console.log(heapSort([0, -11, 15, 10, 9, 1, 2, 3, -13, 14, 4, 5, 6, 7, 8,]));