class Deck {
    constructor(){
        this.cards = 52;
    }
    get count() {
        return this.cards;
    }
    static shuffle() {
        //rearanges 
        return this;
    }
    static draw(n) {
        //remove 
        return n;
    }
}

class Card {
    constructor() {
        this.suit = ['Hearts', 'Diamonds', 'Clubs', 'Spades'];
        this.rank = {
            1: 'Ace',
            2: '2',
            3: '3',
            4: '4',
            5: '5',
            6: '6',
            7: '7',
            8: '8',
            9: '9',
            10: '10',
            11: 'Jack',
            12: 'Queen',
            13: 'King'
        };
    }
    get isFaceCard() {
        if(this.rank>10){
            return true; 
        }
        return false;
    }
    static toString(){
        return `${this.rank} of ${this.suit}`;
    }
    Compare(cardOne, cardTwo){
        if (cardOne > cardTwo){
            return 'one';
        } else if (cardTwo > cardOne){
            return 'two';
        } else if (cardTwo === cardOne) {
            return 'even';
        } else {
            return 'compare error';
        }
    }
}

class Player {
    constructor(name, wins, deck) {
        this.name = name;
        this.wins = wins;
        this.deck = deck;
    }
    Play(playerOne, playerTwo) {
        playerOne = new Player('Dib', 0, 1);
        playerTwo = new Player('Wadya', 0, 1);
        return `${playerOne} ${playerTwo}`;
    }
}

//Task 2

class Employee {
    constructor(id, firstName, lastName, birthday, salary, position, department, age) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.salary = salary;
        this.position = position;
        this.department = department;
        this.age = age;
    }
    static age() {
        return this.age;
    }
    static fullName() {
        return this.firstName + ' ' + this.lastName;
    }
    static EMPLOYEES() {
        return this.firstName + ' ' + this.lastName;
    }
}